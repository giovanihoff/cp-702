"""
    Crie um código em Python capaz de Resolver expressões matemáticas em notação infixa.
    Calcule a expressão de tempo de execução e "big O" do algoritmo em função do número de elementos "(n)"
     da expressão (elemento pode ser um operando ou operador).
    Considere o pior caso para o algoritmo, para calcular o tempo "T(n)", considere ainda que o tempo de
     execução de instruções Python são iguais a uma constante "c".
"""

import time
import unittest


# Função que verifica se um determinado caractere é operador
def __is_operator(character):
    return character in '+-*/'


# Função que verifica qual a maior precedência entre dois (2) operadores determinados
def __higher_precedence(op1, op2):
    # Dicionário que define a precedência dos operadores (neste tive que pesquisar para entender a lógica)
    precedence = {'+': 1, '-': 1, '*': 2, '/': 2}
    return precedence[op1] >= precedence[op2]


# Função que converte uma notação infixa para posfixa (polonesa)
def infix_to_postfix(expression):
    # Pilha que armazena, temporariamente, os operadores durante o processo de conversão
    temporary_stack = []
    # Pilha com o resulado da conversão
    postfix_stack = []
    # Variável para acumular valores com apenas 1 dígito
    number_buffer: str = ''

    # Bem complicado de desenvolver a lógica relacionada ao laço de repetição que segue.
    # Tive que buscar um método de referência para conseguir avançar.
    # Principalmente na questão da precedência e dos parênteses.
    # O restante foi mais tranquilo.
    for character in expression:
        if character.isdigit():  # Se o caractere é um dígito, acumula no buffer
            number_buffer += character
        else:
            if number_buffer:  # Se o buffer não estiver vazio, adiciona o número à pilha posfixa
                postfix_stack.append(number_buffer)
                number_buffer = ''  # Limpa o buffer

            if character.isalnum():  # Se o caractere é um operando (número ou variável)
                postfix_stack.append(character)
            elif __is_operator(character):  # Se o caractere é um operador
                while (temporary_stack and temporary_stack[-1] != '('
                       and __higher_precedence(temporary_stack[-1], character)):
                    postfix_stack.append(temporary_stack.pop())
                temporary_stack.append(character)
            elif character == '(':  # Se o caractere é parênteses à esquerda
                temporary_stack.append(character)
            elif character == ')':  # Se o caractere é parênteses à direita
                while temporary_stack and temporary_stack[-1] != '(':
                    postfix_stack.append(temporary_stack.pop())
                temporary_stack.pop()  # Remove o '('

    if number_buffer:  # Adiciona o último número encontrado, se houver, à pilha posfixa
        postfix_stack.append(number_buffer)

    while temporary_stack:  # Junta a pilha temporária à pilha posfixa
        postfix_stack.append(temporary_stack.pop())

    return ' '.join(postfix_stack)


# Função que resolve uma notação posfixa determinada
def solve_postfix(expression):
    stack = []

    for token in expression.split():
        if token.isdigit():
            stack.append(int(token))
        else:
            operand2 = stack.pop()
            operand1 = stack.pop()

            if token == '+':
                expression_result = operand1 + operand2
            elif token == '-':
                expression_result = operand1 - operand2
            elif token == '*':
                expression_result = operand1 * operand2
            elif token == '/':
                expression_result = operand1 / operand2
            else:
                raise ValueError("Invalid Operator: {}".format(token))

            stack.append(expression_result)

    return str(stack[0])


class TestInfixToPostfix(unittest.TestCase):

    # Teste que apenas verifica se a conversão da notação infixa para posfixa está coerente
    def test_infix_to_postfix(self):
        test_cases = [
            ("A + B * C", "A B C * +"),
            ("(A + B) / (C - D)", "A B + C D - /"),
            ("(A + B) / (C - D)* E", "A B + C D - / E *"),
        ]

        for infix, expected_result in test_cases:
            with self.subTest(postfix=infix, expected_result=expected_result):
                result = infix_to_postfix(infix)
                self.assertEqual(result, expected_result)

    # Teste que valida o desempenho do método para conversão da notação
    def test_performance_infix_to_postfix(self):
        infix = "(((10 + 5) * (8 - 3) / 2) + (7 * 3) - (12 / 4)) * ((15 - 7) / (4 + 2)) + (10 * 3) - (6 / 2)"
        expected_postfix = "10 5 + 8 3 - * 2 / 7 3 * + 12 4 / - 15 7 - 4 2 + / * 10 3 * + 6 2 / -"

        start_time = time.perf_counter()
        postfix = infix_to_postfix(infix)
        end_time = time.perf_counter()
        elapsed_time = end_time - start_time

        self.assertEqual(postfix, expected_postfix)
        print("Tempo de conversão de notação infixa para posfixa: ", elapsed_time)


class TestSolvePostfix(unittest.TestCase):

    # Teste que valida a proposta de trabalho
    def test_solve_postfix(self):
        test_cases = [
            ("(34+8)*5-233/4", "151.75"),
            ("45*(3+7)/5", "90.0"),
            ("((45+(4-9))*2)", "80"),
        ]

        for infix, expected_result in test_cases:
            with self.subTest(infix=infix, expected_postfix=expected_result):
                postfix = infix_to_postfix(infix)
                result = solve_postfix(postfix)
                self.assertEqual(result, expected_result)

    # Teste que valida o desempenho do método para resolução da notação
    def test_performance_solve_postfix(self):
        postfix = "10 5 + 8 3 - * 2 / 7 3 * + 12 4 / - 15 7 - 4 2 + / * 10 3 * + 6 2 / -"
        expected_result = "101.0"

        start_time = time.perf_counter()
        result = solve_postfix(postfix)
        end_time = time.perf_counter()
        elapsed_time = end_time - start_time

        self.assertEqual(result, expected_result)
        print("Tempo de resolução da expressão posfixa: ", elapsed_time)


if __name__ == "__main__":
    unittest.main()
