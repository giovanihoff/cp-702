"""
    Suponha que alguém lhe apresente um saco contendo uma quantidade  ímpar de números,
    seja essa, 2m + 1, com m > 300.000.000.
    Ainda, que lhe seja dito que os números aparecem aos pares, com exceção de um deles.
    Por exemplo, dentro do saco, S, poderia haver S = [[4, 6, 8, 6, 4]].
    Descubra qual é este número diferente.
    Imprima o quanto de tempo e de memória o código criado leva para resolver esse problema.
"""

import random
import time
import sys


def main():
    m = 300000000  # Definindo m
    n = sum(range(2, 2 * m + 1, 2))  # Calculando a soma dos números pares até 2 * m
    numero_diferente = random.randint(1, n + m)  # Escolhendo um número aleatório entre 1 e n+m

    # Garantindo que o número escolhido é realmente diferente
    while numero_diferente % 2 == 0 or numero_diferente in range(2, 2 * m + 1, 2):
        numero_diferente = random.randint(1, n + m)

    # Gerando os números pares
    numeros_pares = gerar_numeros_aleatorios_em_pares(m)

    # Adicionando o número diferente ao conjunto
    conjunto_aleatorio = numeros_pares + [numero_diferente]

    # Embaralhando os números
    random.shuffle(conjunto_aleatorio)

    print("Número diferente injetado no conjunto:")
    print(numero_diferente)

    print("Conjunto de números gerado:")
    print(conjunto_aleatorio)

    numero_encontrado = encontrar_numero_diferente(conjunto_aleatorio)
    print("\nO número diferente encontrado é:", numero_encontrado)


def gerar_numeros_aleatorios_em_pares(tamanho):
    # Gerar uma lista de números aleatórios
    numeros = [random.randint(1, 1000000) for _ in range(tamanho // 2)]

    # Duplicar os números para formar pares
    numeros *= 2

    # Embaralhar a lista
    random.shuffle(numeros)

    return numeros


def gerar_numero_diferente(tamanho):
    # Gerar uma lista de números aleatórios
    numeros = [random.randint(1, 1000000) for _ in range(tamanho // 2)]

    # Duplicar os números para formar pares
    numeros *= 2

    # Embaralhar a lista
    random.shuffle(numeros)

    return numeros


def encontrar_numero_diferente(numeros):
    resultado = 0
    for num in numeros:
        resultado ^= num
    return resultado


if __name__ == "__main__":
    start_time = time.time()
    main()
    end_time = time.time()
    elapsed_time = end_time - start_time
    memory_usage = sys.getsizeof(encontrar_numero_diferente) + sys.getsizeof(main) + sys.getsizeof([])

    print("\nTempo de execução:", elapsed_time, "segundos")
    print("Uso de memória:", memory_usage, "bytes")
