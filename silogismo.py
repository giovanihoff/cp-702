class Silogismo:
    def __init__(self, figura, qualidade, quantidade):
        self.figura = figura
        self.qualidade = qualidade
        self.quantidade = quantidade

    def validar(self):
        # Lista de modos de silogismos válidos
        modos_validos = {
            "AAA": ["1", "2", "3", "4"],
            "AII": ["1", "2", "3", "4"],
            "EAE": ["1", "2", "3", "4"],
            "EIO": ["1", "2", "3", "4"],
            "IAI": ["1", "2", "3", "4"],
            "OAO": ["1", "2", "3", "4"],
            "AEE": ["1", "2", "3", "4"],
            "EAO": ["1", "2", "3", "4"],
            "EAE": ["1", "2", "3", "4"],
            "IAI": ["1", "2", "3", "4"],
            "OAO": ["1", "2", "3", "4"],
            "EIO": ["1", "2", "3", "4"],
            "AOO": ["1", "2", "3", "4"],
            "AEO": ["1", "2", "3", "4"],
            "AII": ["1", "2", "3", "4"],
            "IAO": ["1", "2", "3", "4"],
            "OAI": ["1", "2", "3", "4"],
            "IOA": ["1", "2", "3", "4"],
            "AAI": ["1", "2", "3", "4"],
            "AAO": ["1", "2", "3", "4"],
            "EAA": ["1", "2", "3", "4"],
            "IAA": ["1", "2", "3", "4"],
            "OAA": ["1", "2", "3", "4"],
            "IEE": ["1", "2", "3", "4"],
            "IEI": ["1", "2", "3", "4"],
            "IOO": ["1", "2", "3", "4"],
            "IOI": ["1", "2", "3", "4"],
            "AAI": ["1", "2", "3", "4"]
        }

        modo_atual = self.qualidade + self.quantidade + self.figura
        return modo_atual in modos_validos

# Lista de todas as combinações possíveis de figuras, qualidades e quantidades
figuras = ["I", "II", "III", "IV"]
qualidades = ["A", "E", "I", "O"]
quantidades = ["A", "E", "I", "O"]

# Lista para armazenar silogismos válidos
silogismos_validos = []

# Criando todas as combinações possíveis de silogismos e verificando sua validade
for figura in figuras:
    for qualidade in qualidades:
        for quantidade in quantidades:
            silogismo = Silogismo(figura, qualidade, quantidade)
            if silogismo.validar():
                silogismos_validos.append(silogismo)

# Imprimindo os silogismos válidos
print("Silogismos válidos:")
for silogismo in silogismos_validos:
    print("Figura:", silogismo.figura, "- Qualidade:", silogismo.qualidade, "- Quantidade:", silogismo.quantidade)
