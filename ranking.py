"""
    Suponha que lhe seja pedido para encontrar o ranking de um número inteiro "m" na sequência "L" de "0" até "n",
    seja ele, "r(n)" na qual cadeias com pelo menos dois "1's" consecutivos são proibidos.
    Por exemplo, "r(000) = 1"; "r(001) = 2", "r(010) = 3", "r(011) = 3" e "r(100) = 4".
    Note que "r(011)" contém uma proibição e portanto, "r(010) = r(011) = 3".
"""


def r(n):
    if n == 0:
        return 1
    elif n == 1:
        return 2
    else:
        return r(n - 1) + r(n - 2)


def ranking(m):
    binary_m = bin(m)[2:]
    count = 1
    for i in range(1, len(binary_m)):
        if binary_m[i] == '1' and binary_m[i - 1] == '1':
            return count
        count += r(i)
    return count


if __name__ == "__main__":
    m = 8
    print("O ranking de", m, "é:", ranking(m))
